@extends('layouts.site.master')
@section('content')
<!-- Start About Area -->
<section id="about" class="about-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-12">
                <div class="about-image">
                    <img src="assets/img/about-img1.jpg" alt="about-image">
                    <img src="assets/img/layer.png" class="back-img1" alt="layer">
                    <img src="assets/img/layer.png" class="back-img2" alt="layer">
                    <img src="assets/img/circle-img.jpg" class="circle-img" alt="circle-image">
                </div>
            </div>

            <div class="col-lg-7 col-md-12">
                <div class="about-content">
                    <h3>همه خصوصیات در مورد این ویژگی</h3>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="about-inner">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-building-alt"></i>
                                    </div>
                                    <h4>ساخته شده در سال 1395</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-bed"></i>
                                    </div>
                                    <h4>6 تخت</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-bathtub"></i>
                                    </div>
                                    <h4>5 حمام</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-beach-bed"></i>
                                    </div>
                                    <h4>2 حمام</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-car-alt-4"></i>
                                    </div>
                                    <h4>3 پارکینگ</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-building"></i>
                                    </div>
                                    <h4>2 طبقه</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-hotel"></i>
                                    </div>
                                    <h4>2475 فوت مربع</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="single-inner-box">
                                    <div class="icon">
                                        <i class="icofont-island"></i>
                                    </div>
                                    <h4>0.5 هکتار</h4>
                                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Area -->

<!-- Start Property Details Area -->
<section id="details" class="property-details-area">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col-lg-6 col-md-12 p-0">
                <div class="video-image">
                    <img src="assets/img/video-bg.jpg" alt="image">

                    <a href="../../../https@www.youtube.com/watch@v=bk7McNUjWgw" class="video-btn popup-youtube"><i class="icofont-ui-play"></i></a>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 p-0">
                <div class="property-details-info">
                    <div class="property-details">
                        <h3>جزئیات روشن هستند</h3>

                        <ul>
                            <li>نوع منو: <span>یک خانواده</span></li>
                            <li>شناسه منو: <span>5566128</span></li>
                            <li>اتاق های خواب: <span>5</span></li>
                            <li>حمام: <span>بله</span></li>
                            <li>اندازه متراژ: <span>7،965 فوت مربع </span></li>
                            <li>اندازه بزرگ: <span>8،276.00 فوت مربع </span></li>
                            <li>وضعیت: <span>فعال است</span></li>
                            <li>سال ساخت: <span>1395</span></li>
                            <li>مقیاس: <span>18252.0</span></li>
                            <li>سال مالیاتی: <span>2018</span></li>
                        </ul>
                    </div>

                    <div class="additional-details">
                        <h3>جزئیات بیشتر</h3>

                        <ul>
                            <li>سوخت: <span>برق و گاز</span></li>
                            <li>نوع ساخت و ساز: <span>آجر، قاب</span></li>
                            <li>سایت شومینه: <span>اتاق خواب</span></li>
                            <li>سقف: <span>ترکیب کمربند</span></li>
                            <li>بخاری: <span>چوب</span></li>
                            <li>اندازه بزرگ: <span>8،276.00 فوت مربع</span></li>
                            <li>ویژگی های داخلی: <span>آشپزخانه</span></li>
                            <li>سطح جاده: <span>جاده آسفالت</span></li>
                            <li>فروش اجاره: <span>برای فروش</span></li>
                            <li>فروشی ملکی: <span>در انتظار فروش</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Property Details Area -->

<!-- Start Photo Gallery Area -->
<section id="gallery" class="photo-gallery-area ptb-100 pb-0">
    <div class="container">
        <div class="section-title">
            <h2>گالری عکس</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>
    </div>

    <div class="row m-0">
        <div class="col-lg-6 col-md-12 p-0">
            <div class="photo-gallery-item">
                <a href="assets/img/gallery-img1.jpg" class="popup-btn"><img src="assets/img/gallery-img1.jpg" alt="image"></a>
            </div>
        </div>

        <div class="col-lg-6 col-md-12 p-0">
            <div class="row m-0">
                <div class="col-lg-6 col-sm-6 col-md-6 p-0">
                    <div class="photo-gallery-item">
                        <a href="assets/img/gallery-img2.jpg" class="popup-btn"><img src="assets/img/gallery-img2.jpg" alt="image"></a>
                    </div>
                </div>

                <div class="col-lg-6 col-sm-6 col-md-6 p-0">
                    <div class="photo-gallery-item">
                        <a href="assets/img/gallery-img3.jpg" class="popup-btn"><img src="assets/img/gallery-img3.jpg" alt="image"></a>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-md-12 p-0">
                    <div class="photo-gallery-item">
                        <a href="assets/img/gallery-img4.jpg" class="popup-btn"><img src="assets/img/gallery-img4.jpg" alt="image"></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-12 p-0">
            <div class="photo-gallery-item">
                <a href="assets/img/gallery-img5.jpg" class="popup-btn"><img src="assets/img/gallery-img5.jpg" alt="image"></a>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6 p-0">
            <div class="photo-gallery-item">
                <a href="assets/img/gallery-img6.jpg" class="popup-btn"><img src="assets/img/gallery-img6.jpg" alt="image"></a>
            </div>
        </div>

        <div class="col-lg-3 col-sm-6 col-md-6 p-0">
            <div class="photo-gallery-item">
                <a href="assets/img/gallery-img7.jpg" class="popup-btn"><img src="assets/img/gallery-img7.jpg" alt="image"></a>
            </div>
        </div>
    </div>
</section>
<!-- End Photo Gallery Area -->

<!-- Start Floor Plans Area -->
<section class="floor-plans-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>طرح های ساختمان</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="tab">
                    <ul class="tabs">
                        <li><a href="#">طبقه 1</a></li>

                        <li><a href="#">طبقه 2</a></li>

                        <li><a href="#">طبقه 3</a></li>

                        <li><a href="#">طبقه 4</a></li>

                        <li><a href="#">طبقه 5</a></li>
                    </ul>

                    <div class="tab_content">
                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="assets/img/floor-img1.png" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="assets/img/floor-img2.png" alt="image">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="assets/img/floor-img3.png" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="assets/img/floor-img4.png" alt="image">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs_item">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-md-12 content">
                                    <div class="tabs_item_content">
                                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                                        <ul>
                                            <li>شماره طبقه <span>1</span></li>
                                            <li>اتاق ها <span>4</span></li>
                                            <li>کل منطقه <span>311.50 فوت مربع</span></li>
                                            <li>مزایای آشپزخانه <span>بله</span></li>
                                            <li>پنجره ها <span>4</span></li>
                                        </ul>

                                        <a href="#" class="default-btn">درخواست</a>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-12 image">
                                    <div class="tabs_item_image">
                                        <img src="assets/img/floor-img5.png" alt="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Floor Plans Area -->

<!-- Start Agent Area -->
<section id="agent" class="agent-area ptb-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-12">
                <div class="agent-image">
                    <img src="assets/img/agent.jpg" alt="agent">
                    <img src="assets/img/layer.png" class="back-img1" alt="layer">
                    <img src="assets/img/layer.png" class="back-img2" alt="layer">
                </div>
            </div>

            <div class="col-lg-7 col-md-12">
                <div class="agent-content">
                    <div class="section-title">
                        <h2>گالری عکس</h2>
                        <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    </div>

                    <div class="agent-info">
                        <h3>بریان آدامز</h3>
                        <span>گالری عکس</span>
                    </div>

                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <ul class="agent-contact-info">
                        <li>
                            <span>دفتر مرکزی:</span>
                            <i class="icofont-phone-circle"></i>
                            <a href="#">+444 157 895 4578</a>
                        </li>
                        <li>
                            <span>دفتر:</span>
                            <i class="icofont-fax"></i>
                            <a href="#">+444 157 895 4578</a>
                        </li>
                        <li>
                            <span>ایمیل:</span>
                            <i class="icofont-envelope"></i>
                            <a href="#">Yoursite@Mail.Com</a>
                        </li>
                        <li>
                            <span>نمابر:</span>
                            <i class="icofont-envelope-open"></i>
                            <a href="#">+444 157 895 4578</a>
                        </li>
                    </ul>

                    <ul class="social">
                        <li><a href="#"><i class="icofont-facebook"></i></a></li>
                        <li><a href="#"><i class="icofont-twitter"></i></a></li>
                        <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                        <li><a href="#"><i class="icofont-instagram"></i></a></li>
                    </ul>

                    <a href="#" class="default-btn">تماس بگیرید</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Agent Area -->

<!-- Start Feedback Area -->
<section id="feedback" class="feedback-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>بازخورد مشتری</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-feedback">
                    <div class="client-image">
                        <img src="assets/img/client1.jpg" alt="image">
                    </div>
                    <ul class="rating">
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                    </ul>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="bar"></div>
                    <h3>جان اسمیت</h3>
                    <span>مدیر موسسه</span>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-feedback">
                    <div class="client-image">
                        <img src="assets/img/client2.jpg" alt="image">
                    </div>
                    <ul class="rating">
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                    </ul>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="bar"></div>
                    <h3>جان اسمیت</h3>
                    <span>مدیر موسسه</span>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
                <div class="single-feedback">
                    <div class="client-image">
                        <img src="assets/img/client3.jpg" alt="image">
                    </div>
                    <ul class="rating">
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                        <li><i class="icofont-star"></i></li>
                    </ul>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>

                    <div class="bar"></div>
                    <h3>جان اسمیت</h3>
                    <span>مدیر موسسه</span>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Feedback Area -->

<!-- Start Services Area -->
<section id="services" class="services-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>خدمات ما</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-search-property"></i>
                    </div>
                    <h3>مدیریت املاک</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-chart-pie-alt"></i>
                    </div>
                    <h3>مراقبت از سرمایه</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-chart-histogram-alt"></i>
                    </div>
                    <h3>گزارش مالی</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-flora-flower"></i>
                    </div>
                    <h3>توسعه کسب و کار</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-chart-growth"></i>
                    </div>
                    <h3>وام مسکن</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <div class="icon">
                        <i class="icofont-money"></i>
                    </div>
                    <h3>بازیابی دارایی</h3>
                    <div class="bar"></div>
                    <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                    <a href="#">ادامه خواندن <i class="icofont-bubble-left"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Services Area -->

<!-- Start Map Area -->
<div><iframe src="../../../https@www.google.com/maps/embed@pb=" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe></div>
<!-- End Map Area -->

<!-- Start Contact Area -->
<section id="contact" class="contact-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2>تماس بگیرید</h2>
            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است. لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
        </div>

        <div class="row">
            <div class="col-lg-5 col-md-12">
                <div class="contact-box">
                    <div class="agent-contact">
                        <div class="image">
                            <img src="assets/img/contact-agent.jpg" alt="image">
                        </div>

                        <div class="content">
                            <h3>بریان آدامز</h3>
                            <span>مدیر فروش</span>
                            <p>لورم ایپسوم به راحتی متن ساختاری چاپ و نشر را در بر میگیرد. لورم ایپسوم استاندارد صنعت بوده است.</p>
                        </div>
                    </div>

                    <ul class="contact-info">
                        <li><i class="icofont-phone-circle"></i> <a href="#">+444 157 895 4578</a></li>
                        <li><i class="icofont-fax"></i> <a href="#">+444 157 895 4578</a></li>
                        <li><i class="icofont-envelope"></i> <a href="#">Yoursite@Mail.Com</a></li>
                        <li><i class="icofont-envelope-open"></i> <a href="#">+444 157 895 4578</a></li>
                    </ul>

                    <ul class="social">
                        <li><a href="#"><i class="icofont-facebook"></i></a></li>
                        <li><a href="#"><i class="icofont-twitter"></i></a></li>
                        <li><a href="#"><i class="icofont-linkedin"></i></a></li>
                        <li><a href="#"><i class="icofont-instagram"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-7 col-md-12">
                <form id="contactForm">
                    <div class="row">
                        <div class="col-lg-12 col-md-6">
                            <div class="form-group">
                                <label>نام شما</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="نام خود را وارد کنید" required data-error="لطفا نام خود را وارد کنید">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-6">
                            <div class="form-group">
                                <label>آدرس ایمیل شما</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="آدرس ایمیل خود را وارد کنید" required data-error="لطفا آدرس ایمیل خود را وارد کنید">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>موضوع شما</label>
                                <input type="text" class="form-control" name="msg_subject" id="msg_subject" required data-error="موضوع خود را وارد کنید" placeholder="موضوع خود را وارد کنید">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>پیام شما</label>
                                <textarea placeholder="پیام خود را وارد کنید" name="message" id="message" class="form-control" cols="30" rows="4" required data-error="پیام خود را بنویسید"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <button type="submit" class="default-btn">ارسال پیام</button>
                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End Contact Area -->
@endsection